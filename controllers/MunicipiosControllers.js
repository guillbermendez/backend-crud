const Municipio = require('../models/Municipios');
const Estado= require('../models/Estados');
const { res } = require("express");
const { Op } = require("sequelize");

//Agregar Municipio
exports.agregar = async(req, res, next) => {
    try {
        await Municipio.create(req.body);
        res.json({ mensaje: 'Municipio agregado'});
    } catch (error) {
        console.error(error);
        res.json({ mensaje: 'Erro, Municipio no agregado'});
        next();
    }
};

//listar municipios
exports.listar = async(req, res, next) => {
    try {
        let filtro = req.query;
        if (req.query.q) {
            filtro = { nombre: { [Op.like]: `%${req.query.q}%` } };
        }
        const municipios = await Municipio.findAll({
            where: filtro,
            include: [
                {model: Estado }
            ]
        });
        res.json(municipios);
    } catch (error) {
        console.error(error);
        res.json({ mensaje: 'Error al leer un municipio'});
        next(); 
    }
};
//mostrar / leer por id
exports.mostrar = async(req, res, next) => {
    try {
        const municipios = await Municipio.findByPk(req.params.id);
        if (!municipios) {
            res.status(404).json({ mensaje: 'Municipio no encontrado'});
        } else {
            res.json(municipios);
        }
    } catch (error) {
        res.status(503).json({ mensaje: 'Error al leer el municipio'});
        console.error(error);
    }
};
//Actualizar
exports.actualizar = async(req, res, next) => {
    try {
        const municipios = await Municipio.findByPk(req.params.id);
        if (!municipios) {
            res.status(404).json({ mensaje: 'No se encontro el municipio '});
        } else {

            Object.keys(req.body).forEach((propiedad) => {
                municipios[propiedad] = req.body[propiedad];
            })
            await municipios.save();
            res.json({ mensaje: 'Municipio actualizado'});
        }
    } catch (error) {
        console.error(error);
        let errores = [];
        if (error.errores) {
            errores = error.errores.map((item) => ({
                campo: item.path,
                error: item.message,
            }));
        }
        res.json({ 
            error: true,
            message: 'Error al actualizar el municipio',
            errores,
        });
    }
};
//Eliminar
exports.eliminar = async(req, res, next) => {
    try {
        //Eliminar registro, por id
        const municipios = await Municipio.findByPk(req.params.id);
        if (!municipios) {
            res.status(404).json({ mensaje: 'No se encontro el municipio'});
        } else {
            await municipios.destroy();
            res.json({ mensaje: 'Se elimino el municipio'});
        }
    } catch (error) {
        res.status(503).json({ mensaje: 'Error al eliminar el municipio'});
    }
};

