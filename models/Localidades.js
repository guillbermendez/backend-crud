const Sequelize = require('sequelize');
const db = require('../config/db');
const Municipios = require('./Municipios');

const Localidades = db.define('Localidad', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    nombre: {
        type: Sequelize.STRING(30),
        allowNull: false,
    },
    cp: {
        type: Sequelize.INTEGER,
        allowNull: false,
    }
});

//asociación de pertenencia: Localidad pertenece a Municipio
Municipios.belongsTo(Municipios, {onDelete: 'CASCADE' });

//exportarlo
module.exports = Localidades;