const Sequelize = require('sequelize');
const db = require('../config/db');

const Estados = db.define('Estados', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    nombre: {
        type: Sequelize.STRING(40),
        allowNull: false,
    },
    activo: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
    },
});

//exportarlo
module.exports = Estados;
